﻿namespace WindowsFormsApp7
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.povrsinaDataSet = new WindowsFormsApp7.povrsinaDataSet();
            this.povrsinaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.povrsinaTableAdapter = new WindowsFormsApp7.povrsinaDataSetTableAdapters.PovrsinaTableAdapter();
            this.tableAdapterManager = new WindowsFormsApp7.povrsinaDataSetTableAdapters.TableAdapterManager();
            this.povrsinaBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.povrsinaDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.povrsinaDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.povrsinaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.povrsinaBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.povrsinaDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(59, 398);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Proracun";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(189, 398);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Kontakti";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(529, 356);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 82);
            this.button3.TabIndex = 2;
            this.button3.Text = "Pokazi površinu";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(713, 350);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 88);
            this.button4.TabIndex = 3;
            this.button4.Text = "Dodaj Površinu";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // povrsinaDataSet
            // 
            this.povrsinaDataSet.DataSetName = "povrsinaDataSet";
            this.povrsinaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // povrsinaBindingSource
            // 
            this.povrsinaBindingSource.DataMember = "Povrsina";
            this.povrsinaBindingSource.DataSource = this.povrsinaDataSet;
            // 
            // povrsinaTableAdapter
            // 
            this.povrsinaTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.PovrsinaTableAdapter = this.povrsinaTableAdapter;
            this.tableAdapterManager.UpdateOrder = WindowsFormsApp7.povrsinaDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // povrsinaBindingSource1
            // 
            this.povrsinaBindingSource1.DataMember = "Povrsina";
            this.povrsinaBindingSource1.DataSource = this.povrsinaDataSet;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(21, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(216, 38);
            this.label1.TabIndex = 6;
            this.label1.Text = "Glavni Prozor";
            // 
            // povrsinaDataSetBindingSource
            // 
            this.povrsinaDataSetBindingSource.DataSource = this.povrsinaDataSet;
            this.povrsinaDataSetBindingSource.Position = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(28, 95);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(240, 150);
            this.dataGridView1.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.povrsinaDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.povrsinaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.povrsinaBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.povrsinaDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private povrsinaDataSet povrsinaDataSet;
        private System.Windows.Forms.BindingSource povrsinaBindingSource;
        private povrsinaDataSetTableAdapters.PovrsinaTableAdapter povrsinaTableAdapter;
        private povrsinaDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource povrsinaBindingSource1;
        private System.Windows.Forms.BindingSource povrsinaDataSetBindingSource;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}

