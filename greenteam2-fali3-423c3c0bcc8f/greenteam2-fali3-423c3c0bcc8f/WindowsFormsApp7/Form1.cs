﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp7
{
    public partial class Form1 : Form
    {
        Form2 f2 = new Form2();
        public Form1()
        {
            InitializeComponent();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Event handler. Called by button1 for click events. </summary>
        ///
        /// <remarks>   Lucija Šušnjar, 21/01/2020. </remarks>
        ///
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        private void button1_Click(object sender, EventArgs e)
        {
            
            Form3 formaa3 = new Form3();
            formaa3.Show();
            
        }

        private void povrsinaBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.povrsinaBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.povrsinaDataSet);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'povrsinaDataSet.Povrsina' table. You can move, or remove it, as needed.
            this.povrsinaTableAdapter.Fill(this.povrsinaDataSet.Povrsina);

            dataGridView1.RowCount = 1;
            //dataGridView2.RowCount = 1;

        }
        private void button4_Click(object sender, EventArgs e)
        {
        
        }

        private void povrsinaDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            Form2 formaa2 = new Form2();
            formaa2.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form4 formaa4 = new Form4();
            formaa4.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            String pom1 = f2.getOsoba();
            String[] pom2 = pom1.Split(':');
            int br1 = 0;
            foreach (string p1 in pom2)
            {

                String[] pom3 = p1.Split(',');
                int br2 = 0;
                dataGridView1.RowCount = pom2.Length;
                foreach (string p2 in pom3)
                {

                    dataGridView1.Rows[br1].Cells[br2].Value = p2;
                    // label3.Text += p2 +"("+br1 + "" + br2 +")"+ ",";
                    br2 += 1;
                }

                br1 += 1;
            }
        }
    }
}
