﻿namespace WindowsFormsApp7
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label adresaLabel;
            System.Windows.Forms.Label dimenziaLabel;
            System.Windows.Forms.Label sadržajLabel;
            System.Windows.Forms.Label kategorijaLabel;
            System.Windows.Forms.Label košenjeLabel;
            System.Windows.Forms.Label navodnjavanjeLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.povrsinaBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.povrsinaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.povrsinaDataSet = new WindowsFormsApp7.povrsinaDataSet();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.povrsinaBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.povrsinaTableAdapter = new WindowsFormsApp7.povrsinaDataSetTableAdapters.PovrsinaTableAdapter();
            this.tableAdapterManager = new WindowsFormsApp7.povrsinaDataSetTableAdapters.TableAdapterManager();
            this.adresaTextBox = new System.Windows.Forms.TextBox();
            this.dimenziaTextBox = new System.Windows.Forms.TextBox();
            this.sadržajTextBox = new System.Windows.Forms.TextBox();
            this.kategorijaTextBox = new System.Windows.Forms.TextBox();
            this.košenjeCheckBox = new System.Windows.Forms.CheckBox();
            this.navodnjavanjeCheckBox = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.povrsinaDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.povrsinaBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.button2 = new System.Windows.Forms.Button();
            adresaLabel = new System.Windows.Forms.Label();
            dimenziaLabel = new System.Windows.Forms.Label();
            sadržajLabel = new System.Windows.Forms.Label();
            kategorijaLabel = new System.Windows.Forms.Label();
            košenjeLabel = new System.Windows.Forms.Label();
            navodnjavanjeLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.povrsinaBindingNavigator)).BeginInit();
            this.povrsinaBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.povrsinaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.povrsinaDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.povrsinaDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.povrsinaBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // adresaLabel
            // 
            adresaLabel.AutoSize = true;
            adresaLabel.Location = new System.Drawing.Point(37, 65);
            adresaLabel.Name = "adresaLabel";
            adresaLabel.Size = new System.Drawing.Size(43, 13);
            adresaLabel.TabIndex = 1;
            adresaLabel.Text = "Adresa:";
            // 
            // dimenziaLabel
            // 
            dimenziaLabel.AutoSize = true;
            dimenziaLabel.Location = new System.Drawing.Point(37, 91);
            dimenziaLabel.Name = "dimenziaLabel";
            dimenziaLabel.Size = new System.Drawing.Size(53, 13);
            dimenziaLabel.TabIndex = 3;
            dimenziaLabel.Text = "Dimenzia:";
            // 
            // sadržajLabel
            // 
            sadržajLabel.AutoSize = true;
            sadržajLabel.Location = new System.Drawing.Point(37, 117);
            sadržajLabel.Name = "sadržajLabel";
            sadržajLabel.Size = new System.Drawing.Size(45, 13);
            sadržajLabel.TabIndex = 5;
            sadržajLabel.Text = "Sadržaj:";
            // 
            // kategorijaLabel
            // 
            kategorijaLabel.AutoSize = true;
            kategorijaLabel.Location = new System.Drawing.Point(37, 143);
            kategorijaLabel.Name = "kategorijaLabel";
            kategorijaLabel.Size = new System.Drawing.Size(57, 13);
            kategorijaLabel.TabIndex = 7;
            kategorijaLabel.Text = "Kategorija:";
            // 
            // košenjeLabel
            // 
            košenjeLabel.AutoSize = true;
            košenjeLabel.Location = new System.Drawing.Point(37, 171);
            košenjeLabel.Name = "košenjeLabel";
            košenjeLabel.Size = new System.Drawing.Size(48, 13);
            košenjeLabel.TabIndex = 9;
            košenjeLabel.Text = "Košenje:";
            // 
            // navodnjavanjeLabel
            // 
            navodnjavanjeLabel.AutoSize = true;
            navodnjavanjeLabel.Location = new System.Drawing.Point(37, 201);
            navodnjavanjeLabel.Name = "navodnjavanjeLabel";
            navodnjavanjeLabel.Size = new System.Drawing.Size(82, 13);
            navodnjavanjeLabel.TabIndex = 11;
            navodnjavanjeLabel.Text = "Navodnjavanje:";
            // 
            // povrsinaBindingNavigator
            // 
            this.povrsinaBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.povrsinaBindingNavigator.BindingSource = this.povrsinaBindingSource;
            this.povrsinaBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.povrsinaBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.povrsinaBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.povrsinaBindingNavigatorSaveItem});
            this.povrsinaBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.povrsinaBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.povrsinaBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.povrsinaBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.povrsinaBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.povrsinaBindingNavigator.Name = "povrsinaBindingNavigator";
            this.povrsinaBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.povrsinaBindingNavigator.Size = new System.Drawing.Size(800, 25);
            this.povrsinaBindingNavigator.TabIndex = 0;
            this.povrsinaBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // povrsinaBindingSource
            // 
            this.povrsinaBindingSource.DataMember = "Povrsina";
            this.povrsinaBindingSource.DataSource = this.povrsinaDataSet;
            // 
            // povrsinaDataSet
            // 
            this.povrsinaDataSet.DataSetName = "povrsinaDataSet";
            this.povrsinaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // povrsinaBindingNavigatorSaveItem
            // 
            this.povrsinaBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.povrsinaBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("povrsinaBindingNavigatorSaveItem.Image")));
            this.povrsinaBindingNavigatorSaveItem.Name = "povrsinaBindingNavigatorSaveItem";
            this.povrsinaBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.povrsinaBindingNavigatorSaveItem.Text = "Save Data";
            this.povrsinaBindingNavigatorSaveItem.Click += new System.EventHandler(this.povrsinaBindingNavigatorSaveItem_Click);
            // 
            // povrsinaTableAdapter
            // 
            this.povrsinaTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.PovrsinaTableAdapter = this.povrsinaTableAdapter;
            this.tableAdapterManager.UpdateOrder = WindowsFormsApp7.povrsinaDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // adresaTextBox
            // 
            this.adresaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.povrsinaBindingSource, "Adresa", true));
            this.adresaTextBox.Location = new System.Drawing.Point(125, 62);
            this.adresaTextBox.Name = "adresaTextBox";
            this.adresaTextBox.Size = new System.Drawing.Size(104, 20);
            this.adresaTextBox.TabIndex = 2;
            this.adresaTextBox.TextChanged += new System.EventHandler(this.adresaTextBox_TextChanged);
            // 
            // dimenziaTextBox
            // 
            this.dimenziaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.povrsinaBindingSource, "Dimenzia", true));
            this.dimenziaTextBox.Location = new System.Drawing.Point(125, 88);
            this.dimenziaTextBox.Name = "dimenziaTextBox";
            this.dimenziaTextBox.Size = new System.Drawing.Size(104, 20);
            this.dimenziaTextBox.TabIndex = 4;
            this.dimenziaTextBox.TextChanged += new System.EventHandler(this.dimenziaTextBox_TextChanged);
            // 
            // sadržajTextBox
            // 
            this.sadržajTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.povrsinaBindingSource, "Sadržaj", true));
            this.sadržajTextBox.Location = new System.Drawing.Point(125, 114);
            this.sadržajTextBox.Name = "sadržajTextBox";
            this.sadržajTextBox.Size = new System.Drawing.Size(104, 20);
            this.sadržajTextBox.TabIndex = 6;
            this.sadržajTextBox.TextChanged += new System.EventHandler(this.sadržajTextBox_TextChanged);
            // 
            // kategorijaTextBox
            // 
            this.kategorijaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.povrsinaBindingSource, "Kategorija", true));
            this.kategorijaTextBox.Location = new System.Drawing.Point(125, 140);
            this.kategorijaTextBox.Name = "kategorijaTextBox";
            this.kategorijaTextBox.Size = new System.Drawing.Size(104, 20);
            this.kategorijaTextBox.TabIndex = 8;
            this.kategorijaTextBox.TextChanged += new System.EventHandler(this.kategorijaTextBox_TextChanged_1);
            // 
            // košenjeCheckBox
            // 
            this.košenjeCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.povrsinaBindingSource, "Košenje", true));
            this.košenjeCheckBox.Location = new System.Drawing.Point(125, 166);
            this.košenjeCheckBox.Name = "košenjeCheckBox";
            this.košenjeCheckBox.Size = new System.Drawing.Size(104, 24);
            this.košenjeCheckBox.TabIndex = 10;
            this.košenjeCheckBox.Text = "checkBox1";
            this.košenjeCheckBox.UseVisualStyleBackColor = true;
            // 
            // navodnjavanjeCheckBox
            // 
            this.navodnjavanjeCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.povrsinaBindingSource, "Navodnjavanje", true));
            this.navodnjavanjeCheckBox.Location = new System.Drawing.Point(125, 196);
            this.navodnjavanjeCheckBox.Name = "navodnjavanjeCheckBox";
            this.navodnjavanjeCheckBox.Size = new System.Drawing.Size(104, 24);
            this.navodnjavanjeCheckBox.TabIndex = 12;
            this.navodnjavanjeCheckBox.Text = "checkBox1";
            this.navodnjavanjeCheckBox.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(629, 232);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "Dodaj";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // povrsinaDataGridView
            // 
            this.povrsinaDataGridView.AutoGenerateColumns = false;
            this.povrsinaDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.povrsinaDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewCheckBoxColumn2});
            this.povrsinaDataGridView.DataSource = this.povrsinaBindingSource1;
            this.povrsinaDataGridView.Location = new System.Drawing.Point(28, 301);
            this.povrsinaDataGridView.Name = "povrsinaDataGridView";
            this.povrsinaDataGridView.Size = new System.Drawing.Size(656, 220);
            this.povrsinaDataGridView.TabIndex = 14;
            this.povrsinaDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.povrsinaDataGridView_CellContentClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Adresa";
            this.dataGridViewTextBoxColumn2.HeaderText = "Adresa";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Dimenzia";
            this.dataGridViewTextBoxColumn3.HeaderText = "Dimenzia";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Sadržaj";
            this.dataGridViewTextBoxColumn4.HeaderText = "Sadržaj";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Kategorija";
            this.dataGridViewTextBoxColumn5.HeaderText = "Kategorija";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "Košenje";
            this.dataGridViewCheckBoxColumn1.HeaderText = "Košenje";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.DataPropertyName = "Navodnjavanje";
            this.dataGridViewCheckBoxColumn2.HeaderText = "Navodnjavanje";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            // 
            // povrsinaBindingSource1
            // 
            this.povrsinaBindingSource1.DataMember = "Povrsina";
            this.povrsinaBindingSource1.DataSource = this.povrsinaDataSet;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(486, 232);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 15;
            this.button2.Text = "Ne radi";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 555);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.povrsinaDataGridView);
            this.Controls.Add(this.button1);
            this.Controls.Add(adresaLabel);
            this.Controls.Add(this.adresaTextBox);
            this.Controls.Add(dimenziaLabel);
            this.Controls.Add(this.dimenziaTextBox);
            this.Controls.Add(sadržajLabel);
            this.Controls.Add(this.sadržajTextBox);
            this.Controls.Add(kategorijaLabel);
            this.Controls.Add(this.kategorijaTextBox);
            this.Controls.Add(košenjeLabel);
            this.Controls.Add(this.košenjeCheckBox);
            this.Controls.Add(navodnjavanjeLabel);
            this.Controls.Add(this.navodnjavanjeCheckBox);
            this.Controls.Add(this.povrsinaBindingNavigator);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.povrsinaBindingNavigator)).EndInit();
            this.povrsinaBindingNavigator.ResumeLayout(false);
            this.povrsinaBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.povrsinaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.povrsinaDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.povrsinaDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.povrsinaBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private povrsinaDataSet povrsinaDataSet;
        private System.Windows.Forms.BindingSource povrsinaBindingSource;
        private povrsinaDataSetTableAdapters.PovrsinaTableAdapter povrsinaTableAdapter;
        private povrsinaDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator povrsinaBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton povrsinaBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox adresaTextBox;
        private System.Windows.Forms.TextBox dimenziaTextBox;
        private System.Windows.Forms.TextBox sadržajTextBox;
        private System.Windows.Forms.TextBox kategorijaTextBox;
        private System.Windows.Forms.CheckBox košenjeCheckBox;
        private System.Windows.Forms.CheckBox navodnjavanjeCheckBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView povrsinaDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.BindingSource povrsinaBindingSource1;
        private System.Windows.Forms.Button button2;
    }
}