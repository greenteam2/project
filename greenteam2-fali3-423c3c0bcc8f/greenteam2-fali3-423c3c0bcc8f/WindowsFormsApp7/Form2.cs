﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp7
{
    public partial class Form2 : Form
    {
        Boolean cb1 = false;
        String osoba = "";
        Int32 id = 0001;

        public String getOsoba()
        {
            return osoba;
        }

        public Form2()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            adresaTextBox.Text = "";
            dimenziaTextBox.Text = "";
            kategorijaTextBox.Text = "";
            sadržajTextBox.Text = "";
        }
        private void povrsinaBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.povrsinaBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.povrsinaDataSet);

        }

        private void Form2_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'povrsinaDataSet.Povrsina' table. You can move, or remove it, as needed.
            this.povrsinaTableAdapter.Fill(this.povrsinaDataSet.Povrsina);
            
        }

        private void kategorijaTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void košenjeCheckBox_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {

        }

        private void povrsinaDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void adresaTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        
        private void button1_Click(object sender, EventArgs e)
        {
            if (osoba != "")
            {
                osoba += ":" + id.ToString("0000") + "," + adresaTextBox.Text + "," + dimenziaTextBox.Text + "," + sadržajTextBox.Text+"," + kategorijaTextBox.Text;
            }
            else
            {
                osoba += id.ToString("0000") + "," + adresaTextBox.Text + "," + dimenziaTextBox.Text + "," + sadržajTextBox.Text+","+ kategorijaTextBox.Text;
            }
            id += 1;
            adresaTextBox.Text = "";
            dimenziaTextBox.Text = "";
            kategorijaTextBox.Text = "";
            sadržajTextBox.Text = "";
            this.Hide();


        }

        private void dimenziaTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void sadržajTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void kategorijaTextBox_TextChanged_1(object sender, EventArgs e)
        {

        }
    }
}
